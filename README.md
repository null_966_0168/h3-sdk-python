# 关于

H3Sdk.Python是针对[氚云](https://www.h3yun.com/)低代码平台的官方接口实现的一套非官方SDK，为对接氚云数据开发工作提供方便快捷的请求方式。

<mark>*此版本为此前H3Sdk的Python版本*</mark>

# 教程

## 请求客户端

H3Sdk.Python提供一个请求对象ApiClient，所有请求方法都来自ApiClient。实例化ApiClient需要传入氚云中的EngineCode和EngineSecret

``` python
client = ApiClient(engineCode, engineSecret)
```

## 标准业务请求

基础请求包含氚云标准api中的增删改查和下载附件

### 批量查询业务对象

参数：schemaCode，filter。filter具体用法参考氚云官方文档（这里的filter实际上是仿照氚云后端Api中的filter，用法一样）

``` python
body = {}
filter = Filter()
andMatcher = And()
orMatcher = Or()
orMatcher.add(ItemMatcher("openclose", ComparisonOperatorType.Equal, "手工关闭"))
orMatcher.add(ItemMatcher("openclose", ComparisonOperatorType.Equal, "系统关闭"))
andMatcher.add(orMatcher)
filter.Matcher = andMatcher
result = client.loadBizObjects("D146451jxcworkorder", filter)
```

返回结果，字典列表

``` json
[
    {
        "ObjectId": "6a945b05-7c4d-4ae8-a8c2-f162fc9d7263",
        "Name": "WO2023022401",
        "CreatedBy": "张三",
        "OwnerId": "张三",
        "OwnerDeptId": "张三企业/事业部",
        "CreatedTime": "2023/2/24 14:33:39",
        "ModifiedBy": "张三",
        "ModifiedTime": "2023/2/24 15:03:52",
        "WorkflowInstanceId": "ac15a829-df6b-43ad-bfb5-13d8eec9a01f",
    },
    {
        "ObjectId": "6a945b05-7c4d-4ae8-a8c2-f162fc9d7264",
        "Name": "WO2023022402",
        "CreatedBy": "李四",
        "OwnerId": "李四",
        "OwnerDeptId": "张三企业/事业部",
        "CreatedTime": "2023/2/24 14:33:39",
        "ModifiedBy": "李四",
        "ModifiedTime": "2023/2/24 15:03:52",
        "WorkflowInstanceId": "ac15a829-df6b-43ad-bfb5-13d8eec9a01f",
    }
]
```

### 查询单个业务对象

参数：schemaCode，objectId

``` python
result=client.loadBizObject("D146451jxcworkorder","6a945b05-7c4d-4ae8-a8c2-f162fc9d7263")
```

返回字典

```json
{
    "ObjectId": "6a945b05-7c4d-4ae8-a8c2-f162fc9d7263",
    "Name": "WO2023022401",
    "CreatedBy": "张三",
    "OwnerId": "张三",
    "OwnerDeptId": "张三企业/事业部",
    "CreatedTime": "2023/2/24 14:33:39",
    "ModifiedBy": "张三",
    "ModifiedTime": "2023/2/24 15:03:52",
    "WorkflowInstanceId": "ac15a829-df6b-43ad-bfb5-13d8eec9a01f",
}
```

### 新增业务对象

参数，schemaCode，isSubmit，bizObject。其中isSubmit为true则提交生效，为false表示提交草稿，bizObject填入新增业务对象字段的值

```python
bizObject={
    "supplierName":"AMD"
}
result=client.createBizObject("D146451MyPurchaseOrder",True,bizObject)
```

返回新增的业务对象的objectId

```
e2b07bcb-dede-49f2-b187-5d7b396207e8
```

### 批量新增业务对象

参数：schemaCode，isSubmit，bizObjects。其中，bizObjects为list，传入多个业务对象的字段值

``` python
bizObjects=[
    {"supplierName":"NVIDIA"},
    {"supplierName":"INTEL"}
]
result=client.createBizObjects("D146451MyPurchaseOrder",True,bizObjects)
```

返回新增业务对象的objectId列表

```
['d9bd3eb5-3d72-4b8c-b602-6b5572ea4d01', '17f8595e-4cb4-4472-b8e0-3002cc45c0cf']
```

### 更新业务对象

参数：schemaCode，objectId，bizObject。其中，objectId指定更新的业务对象objectId，bizObject为更新的内容

```python
bizObject={"supplierName":"国美电器S"}
client.updateBizObject("D146451MyPurchaseOrder", "29366d51-a679-4dca-acc6-0c99ae6944a1", bizObject)
```

无返回值

### 删除业务对象

```python
client.removeBizObject("D146451MyPurchaseOrder", "d9bd3eb5-3d72-4b8c-b602-6b5572ea4d01")
```

无返回值

### 下载附件

参数：schemaCode,objectId,bizProperties(字典，参考下面返回值),附件字段名，附件fileId

```python
client.downloadBizObjectFile("D146451attachmentDownload","f549c542-fdf5-446c-b1ea-991f9cd48178",{},"picture","abfba89b-e6e5-4508-a834-977ed34bd093")
```

返回字典

| 字段          | 类型 | 含义                       |
| ------------- | ---- | -------------------------- |
| fileId        | str  | 文件Id                     |
| fileName      | str  | 文件名                     |
| contentType   | str  | 文件类型                   |
| schemaCode    | str  | 业务表单                   |
| bizObjectId   | str  | 业务对象id                 |
| bizProperties | list | 业务对象附加信息，预留字段 |
| fileContent   | list | 文件内容，字节数组         |

```json
{
    "fileId": "abfba89b-e6e5-4508-a834-977ed34bd093",
    "fileName": "pexels-veeterzy-114979.jpg",
    "contentType": "image/jpeg",
    "schemaCode": "D146451attachmentDownload",
    "bizObjectId": "f549c542-fdf5-446c-b1ea-991f9cd48178",
    "bizProperties": null,
    "fileContent": "pSjZT0yUpUXyMUpRvIxSlKXB4G4"
}
```

### 上传附件

参数: schemaCode,objectId,附件字段名，文件流

```python
files={'file': open(filePath,"rb")}
client.uploadBizObjectFile("D146451MyProduct","93484b32-ef2c-41e9-a1ee-10866ac54670","myfile",files)
```

返回附件id



## 自定义接口请求

氚云支持自定义Http接口，因此，根据自定义接口的类型，H3Sdk提供了两种兼容方案。

### 返回ReturnData对象

在氚云后端实现自定义接口的时候，返回标准结果ReturnData，如：

```JSON
{
    "code":0,
    "message":"",
    "data":{}
}
```

> 备注：氚云实现自定义接口请参考官方文档

返回的业务数据均存放于data中，code==0表示执行成功，code==1表示执行异常，异常信息从message中获取

H3Sdk提供PostAsync函数接收自定义接口的情况，并返回ReturnData字典对象