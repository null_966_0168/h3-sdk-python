from enum import IntEnum  
class ComparisonOperatorType(IntEnum):
    Greater=0
    GreaterEqual=1
    Equal=2
    LessEqual=3
    Less=4
    NotEqual=5
    In=6
    NotIn=7    