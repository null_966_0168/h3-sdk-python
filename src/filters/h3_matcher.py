import filters.matcherbase
import filters.matchertype
class Matcher(filters.matcherbase.MatcherBase):
    def __init__(self):
        self.type=filters.matchertype.MatcherType.And
        super().__init__()